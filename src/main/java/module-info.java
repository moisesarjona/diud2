module es.arjona.diud2 {
    requires javafx.controls;
    requires javafx.fxml;
    requires javafx.graphics;

    opens es.arjona.diud2 to javafx.fxml;
    exports es.arjona.diud2;
}