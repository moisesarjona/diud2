package es.arjona.ud02a03;

import javafx.application.Application;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.effect.Effect;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Border;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.text.TextAlignment;
import javafx.stage.Stage;

import java.io.File;

public class Ex1GridPane extends Application {

    @Override
    public void start(Stage stage) throws Exception {
        stage.setTitle("Ex1GridPane");

        GridPane gridPane = new GridPane();
        File file = new File("C:\\Users\\ASUS\\IdeaProjects\\DIUD2\\ud02a03\\src\\main\\resources\\imagenLinux.png");
        String localUrl = file.toURI().toURL().toString();
        Image image = new Image(localUrl);
        ImageView imageView = new ImageView(image);
        imageView.setFitHeight(75);
        imageView.setFitWidth(75);

        gridPane.setPadding(new Insets(20,0,20,20));

        Label label = new Label();
        label.setText("Nom: ");
        GridPane.setHalignment(label, HPos.RIGHT);
        Label label1 = new Label();
        label1.setText("Cognom: ");
        GridPane.setHalignment(label1, HPos.RIGHT);
        Label label2 = new Label();
        label2.setText("Ciutat: ");
        GridPane.setHalignment(label2, HPos.RIGHT);
        Label label3 = new Label();
        label3.setText("Direccio: ");
        GridPane.setHalignment(label3, HPos.RIGHT);
        Label label4 = new Label();
        label4.setText("Descripcio: ");
        GridPane.setHalignment(label4, HPos.RIGHT);

        TextField textField = new TextField();
        TextField textField1 = new TextField();
        TextField textField2 = new TextField();
        TextField textField3 = new TextField();
        TextField textField4 = new TextField();
        TextArea textArea5 = new TextArea();
        textField.setPrefHeight(5);
        textField1.setPrefHeight(5);
        textField2.setPrefHeight(5);
        textField3.setPrefHeight(5);
        textField4.setPrefHeight(5);
        textField4.setMaxSize(40,40);
        textArea5.setPrefWidth(200);
        textArea5.setPrefHeight(100);

        gridPane.setVgap(7);
        gridPane.setHgap(7);

        GridPane.setRowSpan(imageView,3);
        GridPane.setColumnSpan(imageView,2);
        GridPane.setColumnSpan(textArea5,3);

        gridPane.add(label, 0, 0);
        gridPane.add(textField,1,0);
        gridPane.add(label1,0,1);
        gridPane.add(textField1,1,1);
        gridPane.add(label2,0,2);
        gridPane.add(textField2,1,2);
        gridPane.add(label3,0,3);
        gridPane.add(textField3,1,3);
        gridPane.add(textField4,3,3);
        gridPane.add(label4,0,4);
        gridPane.add(textArea5, 1, 4);
        gridPane.add(imageView,2,0);


        Scene scene = new Scene(gridPane, 375, 300);
        stage.setScene(scene);
        stage.show();
    }

    public static void main(String[] args) {
        launch();
    }

}
