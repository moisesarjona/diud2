package es.arjona.ud02a03;

import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.HBox;
import javafx.stage.Stage;

public class Ex4HBox extends Application {

    @Override
    public void start(Stage stage) throws Exception {
        HBox hBox = new HBox();
        hBox.setPadding(new Insets(20,20,20,20));

        Label label = new Label("Etiqueta HBOX");

        Button button1 = new Button("Button1 HBOX");
        button1.setMinSize(125,125);

        Button button2 = new Button("Button2 HBOX");

        TextField textField = new TextField("TextField HBOX");

        RadioButton radioButton = new RadioButton("RadioButton HBOX");

        CheckBox checkBox = new CheckBox("CheckBox HBOX");

        hBox.getChildren().addAll(label,button1,button2,textField,radioButton,checkBox);

        Scene scene = new Scene(hBox, 800, 300);
        stage.setTitle("Exercici HBOX (DI)");
        stage.setScene(scene);
        stage.show();
    }

    public static void main(String[] args) {
        launch(args);
    }

}

