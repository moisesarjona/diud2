package es.arjona.ud02a03;

import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;

public class Ex2BorderPane extends Application {

    @Override
    public void start(Stage stage) throws Exception {
        stage.setTitle("BorderPane");

        BorderPane borderPane = new BorderPane();
        borderPane.setPadding(new Insets(15,20,10,10));


        Button top = new Button();
        top.setText("Situado en la parte superior");
        Button left = new Button();
        left.setText("Situado en la parte izquierda");
        Button center = new Button();
        center.setText("Situado en el centro");
        Button right = new Button();
        right.setText("Situado en la parte derecha");
        Button bottom = new Button();
        bottom.setText("Situado en la parte inferior");

        borderPane.setTop(top);
        borderPane.setLeft(left);
        borderPane.setCenter(center);
        borderPane.setRight(right);
        borderPane.setBottom(bottom);
        BorderPane.setMargin(top, new Insets(10,10,10,10));
        BorderPane.setMargin(left, new Insets(10,10,10,10));
        BorderPane.setMargin(right, new Insets(10,10,10,10));
        BorderPane.setMargin(center, new Insets(10,10,10,10));
        BorderPane.setMargin(bottom, new Insets(10,10,10,10));


        Scene scene = new Scene(borderPane, 900, 300);
        stage.setScene(scene);
        stage.show();
    }

    public static void main(String[] args) {
        launch();
    }
}
