package es.arjona.ud02a03;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;

public class Ex3AnchorPane extends Application {

    @Override
    public void start(Stage stage) throws Exception {
        stage.setTitle("AnchorPane");

        AnchorPane anchorPane = new AnchorPane();

        Button top = new Button("Anclado en la posicion superior");
        AnchorPane.setTopAnchor(top,40d);
        AnchorPane.setLeftAnchor(top,50d);

        Button left = new Button("Anclado a la izquierda");
        AnchorPane.setTopAnchor(left,90d);
        AnchorPane.setLeftAnchor(left,10d);
        AnchorPane.setRightAnchor(left,320d);

        Button right = new Button("Anclado a la derecha");
        AnchorPane.setTopAnchor(right,100d);
        AnchorPane.setRightAnchor(right,20d);

        Button bottom = new Button("Anclado en la posicion inferior");
        AnchorPane.setTopAnchor(bottom,150d);
        AnchorPane.setLeftAnchor(bottom,40d);
        AnchorPane.setRightAnchor(bottom,50d);
        AnchorPane.setBottomAnchor(bottom,45d);

        anchorPane.getChildren().addAll(top,left,right,bottom);
        Scene scene = new Scene(anchorPane, 900, 300);
        stage.setScene(scene);
        stage.show();

    }

    public static void main(String[] args) {
        launch();
    }
}
