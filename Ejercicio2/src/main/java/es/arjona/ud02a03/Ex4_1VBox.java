package es.arjona.ud02a03;

import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;


public class Ex4_1VBox extends Application {
    @Override
    public void start(Stage stage) throws Exception {
        VBox vBox = new VBox();
        vBox.setPadding(new Insets(20,20,20,20));

        Label label = new Label("Etiqueta VBOX");

        Button button1 = new Button("Button1 VBOX");
        button1.setMinSize(125,125);

        Button button2 = new Button("Button2 VBOX");

        TextField textField = new TextField("TextField VBOX");

        RadioButton radioButton = new RadioButton("RadioButton VBOX");

        CheckBox checkBox = new CheckBox("CheckBox VBOX");

        vBox.getChildren().addAll(label,button1,button2,textField,radioButton,checkBox);
        Scene scene = new Scene(vBox, 500, 300);
        stage.setTitle("Exercici 4.1 VBOX");
        stage.setScene(scene);
        stage.show();
    }

    public static void main(String[] args) {
        launch(args);
    }

}
