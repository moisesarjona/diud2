package es.arjona.ejercicio3;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.IOException;

public class Hub extends Application {

        @Override
        public void start(Stage stage) throws IOException {
            FXMLLoader fxmlLoader = new FXMLLoader(Hub.class.getResource("HubInicio.fxml"));
            Scene scene = new Scene(fxmlLoader.load(), 600, 400);
            stage.setTitle("HUB APLICACIONES");
            stage.setScene(scene);
            stage.show();
            stage.setResizable(false);
        }

        public static void main(String[] args) {
            launch();
        }
    }
