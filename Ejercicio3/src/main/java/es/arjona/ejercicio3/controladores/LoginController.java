package es.arjona.ejercicio3.controladores;

import javafx.fxml.FXML;
import javafx.scene.control.TextField;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;

public class LoginController {
    @FXML
    private Text textoBienvenida;

    @FXML
    private TextField usuarioTextField;

    @FXML
    private TextField pswdTextField;

    @FXML
    protected void onIniciarClick () {
        textoBienvenida.setVisible(true);

        String usuarioText = usuarioTextField.getText();
        String pswdText = pswdTextField.getText();

            if (usuarioText.equalsIgnoreCase("batoi") && pswdText.equalsIgnoreCase("1234")) {
                textoBienvenida.setText("Bienvenido");
                textoBienvenida.setStroke(Color.CADETBLUE);
                textoBienvenida.setVisible(true);
            } else {
                textoBienvenida.setText("INICIO DE SESION ERRONEO");
                textoBienvenida.setStroke(Color.INDIANRED);
                textoBienvenida.setVisible(true);
            }

    }
}
