package es.arjona.ejercicio3.controladores;

import es.arjona.ejercicio3.Calculator;
import es.arjona.ejercicio3.Formulario;
import es.arjona.ejercicio3.LoginApp;
import javafx.event.ActionEvent;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.IOException;

public class HubController {

    public void eventE1(ActionEvent actionEvent) throws IOException {

        Stage stage= new Stage();
        FXMLLoader fxmlLoader = new FXMLLoader(LoginApp.class.getResource("ejercicio3Login.fxml"));
        Scene scene = new Scene(fxmlLoader.load(), 600, 400);
        stage.setTitle("Login");
        stage.setScene(scene);
        stage.show();
        stage.setResizable(false);

    }

    public void eventE2(ActionEvent actionEvent) throws IOException {

        Stage stage= new Stage();
        FXMLLoader fxmlLoader = new FXMLLoader(Calculator.class.getResource("Calculator.fxml"));
        Scene scene = new Scene(fxmlLoader.load(), 230, 315);
        stage.setTitle("Calculadora");
        stage.setScene(scene);
        stage.show();
        stage.setResizable(false);


    }

    public void eventE3(ActionEvent actionEvent) throws IOException {
        Stage stage= new Stage();
        FXMLLoader fxmlLoader = new FXMLLoader(Formulario.class.getResource("formulario.fxml"));
        Scene scene = new Scene(fxmlLoader.load(), 250, 400 );
        stage.setTitle("Formulario");
        stage.setScene(scene);
        stage.show();
        stage.setResizable(false);

    }
}