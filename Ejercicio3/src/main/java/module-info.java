module es.arjona.ejercicio3 {
    requires javafx.controls;
    requires javafx.fxml;


    opens es.arjona.ejercicio3 to javafx.fxml;
    exports es.arjona.ejercicio3;
    exports es.arjona.ejercicio3.controladores;
    opens es.arjona.ejercicio3.controladores to javafx.fxml;
}